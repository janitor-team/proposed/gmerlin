Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gmerlin
Upstream-Contact: Members of the Gmerlin project <gmerlin-general@lists.sourceforge.net>
Source: http://gmerlin.sourceforge.net/
Files-Excluded:
 include/gmerlin/bg_sem.h
 doc/*

Files: *
Copyright:
 2001-2022, Members of the Gmerlin project <gmerlin-general@lists.sourceforge.net>
 2006, Burkhard Plaum
License: GPL-3+

Files:
 apps/*/*.[ch]
 include/*.h
 lib/*.c
 plugins/*/*.[ch]
 icons/*.png
Copyright:
 2001-2022, Members of the Gmerlin project <gmerlingeneral@lists.sourceforge.net>
 2006, Michael Niedermayer <michaelni@gmx.at>
 2007, Alexander G. Balakhnin aka Fizick
 2004, Luc Saillard (luc@saillard.org)
 2003, Jakub 'jimmac' Steiner
 2001-2004, Nemosoft Unv
License: GPL-2+

Files: config.rpath
 m4/*.m4
Copyright: 1995-2006, Free Software Foundation, Inc
License: other-GAP

Files: plugins/image/targa.[ch]
Copyright: 2001-2003, Emil Mikulic
License: other-targa

Files: lib/uthread_sem.c
Copyright: 2000, Jason Evans <jasone@freebsd.org>
License: BSD-2-clause

Files: include/float_cast.h include/ladspa.h
Copyright:
 2000-2002, Richard W.E. Furse, Paul Barton-Davis
 2001-2002, Erik de Castro Lopo <erikd@zip.com.au>
License: LGPL-2.1+

Files: po/Makefile.in.in
Copyright: 1995-1997, 2000-2006, Ulrich Drepper <drepper@gnu.ai.mit.edu>
License: other-GAP-gettext

Files: plugins/cdaudio/sha1.[ch]
Copyright: - (2001 The Bitzi Corporation, Uwe Hollerbach)
License: bitzi-public-domain

Files: debian/*
Copyright: 2008, Christian Marillat <marillat@debian.org>,
 2010, Romain Beauxis <toots@rastageeks.org>,
 2010-2011, Alessio Treglia <alessio@debian.org>,
 2011-2022, IOhannes m zmölnig <umlaeute@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2".
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in "/usr/share/common-licenses/GPL-3".
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in '/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice(s), this list of conditions and the following disclaimer as
    the first lines of this file unmodified other than the possible
    addition of one or more copyright notices.
 2. Redistributions in binary form must reproduce the above copyright
    notice(s), this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER(S) ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER(S) BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: other-GAP
 This file is free software; the Free Software Foundation gives
 unlimited permission to copy and/or distribute it, with or without
 modifications, as long as this notice is preserved.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.
 .
 Some files differ from above by replacing "This file" with "This
 Makefile.in".

License: other-GAP-gettext
 This file may be copied and used freely without restrictions.  It may
 be used in projects which are not available under a GNU General Public
 License, but which still want to provide support for the GNU gettext
 functionality.

License: other-targa
 Source and binary redistribution of this code, with or without changes, for
 free or for profit, is allowed as long as this copyright notice is kept
 intact.  Modified versions must be clearly marked as modified.
 .
 This code is provided without any warranty.  The copyright holder is
 not liable for anything bad that might happen as a result of the code.

License: bitzi-public-domain
 1. Authorship. This work and others bearing the above
    label were created by, or on behalf of, the Bitzi
    Corporation. Often other public domain material by
    other authors is incorporated; this should be clear
    from notations in the source code. If other non-
    public-domain code or libraries are included, this is
    is done under those works' respective licenses.
 2. Release. The Bitzi Corporation places its portion
    of these labelled works into the public domain,
    disclaiming all rights granted us by copyright law.
    Bitzi places no restrictions on your freedom to copy,
    use, redistribute and modify this work, though you
    should be aware of points (3), (4), and (5) below.
 3. Trademark Advisory. The Bitzi Corporation reserves
    all rights with regard to any of its trademarks which
    may appear herein, such as "Bitzi", "Bitcollider", or
    "Bitpedia". Please take care that your uses of this
    work do not infringe on our trademarks or imply our
    endorsement. For example, you should change labels
    and identifier strings in your derivative works where
    appropriate.
 4. Licensed portions. Some code and libraries may be
    incorporated in this work in accordance with the
    licenses offered by their respective rightsholders.
    Further copying, use, redistribution and modification
    of these third-party portions remains subject to
    their original licenses.
 5. Disclaimer. THIS SOFTWARE IS PROVIDED BY THE AUTHOR
    ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
    TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 Please see http://bitzi.com/publicdomain or write
 info@bitzi.com for more info.
